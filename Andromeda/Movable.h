#pragma once

#ifndef MOVABLE_H
#define MOVABLE_H

class Movable
{
protected:
	int posX;
	int posY;

public:
	Movable() : posX(0), posY(0) {
	
	};
	virtual ~Movable();
	int getPosX();
	void setPosX(const int posX);
	int getPosY();
	void setPosY(const int posY);
};
#endif
