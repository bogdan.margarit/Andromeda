#include "Movable.h"

Movable::~Movable()
{

}

int Movable::getPosX()
{
	return posX;
}

void Movable::setPosX(const int posX)
{
	this->posX = posX;
}

int Movable::getPosY()
{
	return posY;
}

void Movable::setPosY(const int posY)
{
	this->posY = posY;
}