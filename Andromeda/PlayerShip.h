#pragma once

#ifndef PLAYERSHIP_H
#define PLAYERSHIP_H

#include <SDL.h>
#include "Movable.h"
#include <vector>
#include "Gun.h"

class PlayerShip : public Movable
{
private:
	Gun * currentGun;
	//std::vector<Gun*> gunInventory;
	
public:
	PlayerShip();
	~PlayerShip() override {
		delete currentGun;
	};
	Gun * getCurrentGun();
	void draw(SDL_Renderer& renderer);
};

#endif