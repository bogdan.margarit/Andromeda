#include "Engine.h"
#include <stdio.h>


/**
 * Constructor
 */
Engine::Engine()
{
	gameState = GameState::IN_MENU;
	initSDL();

	playerShip = new PlayerShip();
}

/**
 * Destructor
 */
Engine::~Engine()
{
	delete playerShip;
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

/**
 * Get current display resolution and set screen size.
 */
void Engine::initScreenSize()
{
	SDL_DisplayMode displayMode;
	SDL_GetDisplayMode(0, 0, &displayMode);
	screenWidth = displayMode.w / 1.5f;
	screenHeight = displayMode.h / 1.2f;
}

void Engine::initSDL()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		printf("SDL initialization error: %s\n", SDL_GetError());
	}

	initScreenSize();

	// Initialize SDL window
	window = SDL_CreateWindow(
		"Andromeda v0.1 ALPHA",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		screenWidth,
		screenHeight,
		SDL_WINDOW_SHOWN
	);

	if (window == nullptr) {
		printf("SDL window initialization error: %s\n", SDL_GetError());
	}

	// Initialize the renderer
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

/**
 * Run the game.
 */
void Engine::run()
{
	loop();
}

/**
 * Start the game loop.
 */
void Engine::loop()
{
	// Keep looping untill exit signal is received.
	while (gameState != GameState::EXIT_SIGNAL) {
		update();
		render();
	}
}

void Engine::getInput()
{
	SDL_Event event;
	while (true == SDL_PollEvent(&event)) {
		handleInput(&event);
	}
}

/**
 * Update the game.
 */
void Engine::update()
{
	getInput();
}

/** 
 * Render the game.
 */
void Engine::render()
{
	playerShip->draw(*renderer);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderPresent(renderer);
	SDL_RenderClear(renderer);
}

void Engine::handleInput(SDL_Event const* event)
{
	switch (event->type) {
	case SDL_QUIT:
		gameState = GameState::EXIT_SIGNAL;
		break;
	case SDL_KEYDOWN:
		int keyPressed = event->key.keysym.sym;
		if (keyPressed == SDLK_LEFT) {
			playerShip->setPosX(playerShip->getPosX() - 5);
		}
		else if (keyPressed == SDLK_RIGHT) {
			playerShip->setPosX(playerShip->getPosX() + 5);
		}
		else if (keyPressed == SDLK_UP) {
			playerShip->setPosY(playerShip->getPosY() - 5);
		}
		else if (keyPressed == SDLK_DOWN) {
			playerShip->setPosY(playerShip->getPosY() + 5);
		}
		else if (keyPressed == SDLK_x) {
			playerShip->getCurrentGun()->fire(*renderer);
		}
		else if (keyPressed == SDLK_z) {
			
		}
		break;
	}
}