#include "PlayerShip.h"

PlayerShip::PlayerShip()
{
	currentGun = new Gun();
}

Gun * PlayerShip::getCurrentGun()
{
	return currentGun;
}

void PlayerShip::draw(SDL_Renderer& renderer)
{
	

	SDL_Rect rectangle;
	rectangle.x = posX;
	rectangle.y = posY;
	rectangle.w = 32;
	rectangle.h = 32;

	SDL_SetRenderDrawColor(&renderer, 0, 0, 255, 255);
	SDL_RenderFillRect(&renderer, &rectangle);
	SDL_RenderPresent(&renderer);
}