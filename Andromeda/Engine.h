#pragma once

#ifndef ENGINE_H
#define ENGINE_H

#include <SDL.h>
#include <SDL_image.h>
#include "PlayerShip.h"
#include <typeinfo>

/**
 * Possible game states.
 */
enum class GameState
{
	EXIT_SIGNAL, 
	RUNNING, 
	PAUSED, 
	IN_MENU
};

/**
 * Game engine/manager.
 */
class Engine
{
private:
	// Properties
	int screenWidth;
	int screenHeight;

	GameState gameState;
	SDL_Window * window;
	SDL_Renderer * renderer;

	PlayerShip * playerShip;
	
	// Methods
	void initSDL();
	void initScreenSize();
	void loop();
	void getInput();
public:
	// Methods
	Engine();
	~Engine();
	void handleInput(SDL_Event const* event);
	void update();
	void render();
	void run();
};

#endif